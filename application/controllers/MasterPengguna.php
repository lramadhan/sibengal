<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterPengguna extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Master Pengguna",
				"konten" => "masterPengguna/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file MasterPengguna.php */
/* Location: ./application/controllers/MasterPengguna.php */