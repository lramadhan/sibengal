<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanHakAkses extends CI_Controller {
	public function __construct() {
		parent ::__construct();
		$this->load->model('web');
	}

	public function index()
	{
		$data =["judul" => "Pengaturan Hak Akses",
				"konten" => "pengaturanHakAkses/index",
				"data_buku" => $this->web->get_all_buku()];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file PengaturanHakAkses.php */
/* Location: ./application/controllers/PengaturanHakAkses.php */