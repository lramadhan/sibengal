<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaPerawatanBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Kelola Perawatan Barang",
				"konten" => "kelolaPerawatanBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file KelolaPerawatanBarang.php */
/* Location: ./application/controllers/KelolaPerawatanBarang.php */