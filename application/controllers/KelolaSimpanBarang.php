<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaSimpanBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Kelola Simpan Barang",
				"konten" => "kelolaSimpanBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file KelolaSimpanBarang.php */
/* Location: ./application/controllers/KelolaSimpanBarang.php */