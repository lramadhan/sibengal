<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterSatuan extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Master Satuan",
				"konten" => "masterSatuan/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file MasterSatuan.php */
/* Location: ./application/controllers/MasterSatuan.php */