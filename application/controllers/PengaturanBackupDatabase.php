<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanBackupDatabase extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Cadangkan Basis Data",
				"konten" => "pengaturanBackupDatabase/index"];

		$this->load->view('wrapper/layout', $data);
	}

	public function backdb()
	{
		$this->load->dbutil();

		$aturan	= ['format' => "zip",
					'filename' => "my_db_backup.sql"];

		$backup =& $this->dbutil->backup($aturan);

		$namadb = 'database-sibengal-pada-'. date("Y-m-d-H-i-s") .'.zip';

		$simpan = '/backup'.$namadb;

		$this->load->helper('file');
		write_file($simpan, $backup);

		$this->load->helper('download');
		force_download($namadb, $backup);
	}

}

/* End of file PengaturanBackupDatabase.php */
/* Location: ./application/controllers/PengaturanBackupDatabase.php */