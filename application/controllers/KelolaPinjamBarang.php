<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaPinjamBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Kelola Pinjam Barang",
				"konten" => "kelolaPinjamBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file KelolaPinjamBarang.php */
/* Location: ./application/controllers/KelolaPinjamBarang.php */