<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterSumberDana extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Master Sumber Dana",
				"konten" => "masterSumberDana/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file MasterSumberDana.php */
/* Location: ./application/controllers/MasterSumberDana.php */