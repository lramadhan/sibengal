<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterTempatPenyimpanan extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Master Tempat Penyimpanan",
				"konten" => "masterTempatPenyimpanan/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file MasterTempatPenyimpan.php */
/* Location: ./application/controllers/MasterTempatPenyimpan.php */