<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaMutasiBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Kelola Mutasi Barang",
				"konten" => "kelolaMutasiBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file kelolaMutasiBarang.php */
/* Location: ./application/controllers/kelolaMutasiBarang.php */