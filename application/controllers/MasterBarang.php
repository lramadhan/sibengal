<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Master Barang",
				"konten" => "masterBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file MasterBarang.php */
/* Location: ./application/controllers/MasterBarang.php */