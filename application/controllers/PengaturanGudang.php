<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanGudang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Pengaturan Gudang",
				"konten" => "pengaturanGudang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file PengaturanGudang.php */
/* Location: ./application/controllers/PengaturanGudang.php */