<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanPerawatan extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Laporan Perawatan",
				"konten" => "laporanPerawatan/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file LaporanPerawatan.php */
/* Location: ./application/controllers/LaporanPerawatan.php */