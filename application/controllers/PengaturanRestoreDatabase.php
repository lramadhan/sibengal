<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanRestoreDatabase extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Pulihkan Basis Data",
				"konten" => "pengaturanRestoreDatabase/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file PengaturanRestoreDatabase.php */
/* Location: ./application/controllers/PengaturanRestoreDatabase.php */