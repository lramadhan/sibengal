<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanPeminjaman extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Laporan Peminjaman",
				"konten" => "laporanPeminjaman/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file LaporanPeminjaman.php */
/* Location: ./application/controllers/LaporanPeminjaman.php */