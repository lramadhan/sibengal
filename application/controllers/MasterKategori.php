<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterKategori extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Master Kategori",
				"konten" => "masterKategori/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file MasterKategori.php */
/* Location: ./application/controllers/MasterKategori.php */