<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaHapusBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Kelola Hapus Barang",
				"konten" => "kelolaHapusBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file KelolaHapusBarang.php */
/* Location: ./application/controllers/KelolaHapusBarang.php */