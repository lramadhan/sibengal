<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterPerawatan extends CI_Controller {

  public function index()
  {
    $data = ["judul" => "Status Perawatan",
            "konten" => "masterPerawatan/index"];

    $this->load->view('wrapper/layout', $data);
  }

}

/* End of file MasterPerawatan.php */
/* Location: ./application/controllers/MasterPerawatan.php */