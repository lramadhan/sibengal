<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Profil",
				"konten" => "profile/index"];
		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */