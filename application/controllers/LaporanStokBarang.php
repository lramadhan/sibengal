<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanStokBarang extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Laporan Stok Barang",
				"konten" => "laporanStokBarang/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file LaporanStokBarang.php */
/* Location: ./application/controllers/LaporanStokBarang.php */