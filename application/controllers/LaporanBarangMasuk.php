<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanBarangMasuk extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Laporan Barang Masuk",
				"konten" => "laporanBarangMasuk/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file LaporanBarangMasuk.php */
/* Location: ./application/controllers/LaporanBarangMasuk.php */