<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanPengguna extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Laporan Pengguna",
				"konten" => "laporanPengguna/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file LaporanPengguna.php */
/* Location: ./application/controllers/LaporanPengguna.php */