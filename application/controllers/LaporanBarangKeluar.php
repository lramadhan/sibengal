<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanBarangKeluar extends CI_Controller {

	public function index()
	{
		$data =["judul" => "Laporan Barang Keluar",
				"konten" => "laporanBarangKeluar/index"];

		$this->load->view('wrapper/layout', $data);
	}

}

/* End of file LaporanBarangKeluar.php */
/* Location: ./application/controllers/LaporanBarangKeluar.php */