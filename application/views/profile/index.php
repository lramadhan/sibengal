<div class="card mb-4">
  <!-- judul kartu -->
    <div class="card-header bg-white font-weight-bold">
        Data Lengkap Orang
    </div>
  <!-- Isi kartu -->
    <div class="card-body">
      <div class="row">
      <div class="col-md-3 col-lg-3 control" align="center">
        <img alt="Foto Profil" src="https://www.planwallpaper.com/static/images/96d186ef9ae1d063b50bc1d9a03af5cc--mobile-wallpaper-photo-wallpaper.jpg" class="img-thumbnail img-fluid" width="200" height="200">
        <label>Ganti Foto <input type="file" name="fotoProfil" class="form-control-file"></label>
        <button type="button" class="btn btn-outline-danger">Unggah</button>
      </div>
      <div class=" col-md-9 col-lg-9 "> 
        <table class="table table-user-information">
          <tbody>
            <tr>
              <td>Department:</td>
              <td>Programming</td>
            </tr>
            <tr>
              <td>Hire date:</td>
              <td>06/23/2013</td>
            </tr>
            <tr>
              <td>Date of Birth</td>
              <td>01/24/1988</td>
            </tr>
         
               <tr>
                   <tr>
              <td>Gender</td>
              <td>Female</td>
            </tr>
              <tr>
              <td>Home Address</td>
              <td>Kathmandu,Nepal</td>
            </tr>
            <tr>
              <td>Email</td>
              <td><a href="mailto:info@support.com">info@support.com</a></td>
            </tr>
              <td>Phone Number</td>
              <td>123-4567-890(Landline)<br><br>555-4567-890(Mobile)
              </td>
            </tr>
           
          </tbody>
        </table>
      </div>
      </div>
    </div>
    <!-- Kaki kartu -->
    <div class="card-footer bg-white modal-footer">
        <a href="#" class="btn btn-warning"><i class="fa fa-fw fa-edit"></i> Ubah Data</a>
    </div>
</div>

<div class="card mb-4">
  <div class="card-header bg-white font-weight-bold">
      Username dan Password Login
  </div>
  <div class="card-body">
      <form class="form-inline">
          <label class="sr-only" for="username">Username</label>
          <input type="text" class="form-control mr-sm-2" id="username" placeholder="$username">

          <label class="sr-only" for="password">Password</label>
          <div class="input-group mr-sm-2">
              <input type="text" class="form-control" id="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-warning">Perbarui</button>
      </form>
  </div>
