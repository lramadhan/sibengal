<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>SIBENGAL | Login</title>
	<link rel="stylesheet" href=" <?php echo base_url('assets/css/auth.css'); ?> ">
</head>

<body>
	<div class="lowin lowin-blue">
		<div class="lowin-brand">
			<img src="<?php echo base_url('assets/image/logo.png') ?>" alt="logo">
		</div>
		<div class="lowin-wrapper">
			<div class="lowin-box lowin-login">
				<div class="lowin-box-inner">
					<form>
						<p>SIBENGAL v.0.0.8</p>
						<div class="lowin-group">
							<label>Email <a href="#" class="login-back-link">Masuk?</a></label>
							<input type="email" autocomplete="email" name="email" class="lowin-input">
						</div>
						<div class="lowin-group password-group">
							<label>Password <a href="#" class="forgot-link">Lupa Password?</a></label>
							<input type="password" name="password" autocomplete="current-password" class="lowin-input">
						</div>
						<button class="lowin-btn login-btn">
							Masuk
						</button>

						<div class="text-foot">
							Tidak memiliki akun? <a href="" class="register-link">Daftar</a>
						</div>
					</form>
				</div>
			</div>

			<div class="lowin-box lowin-register">
				<div class="lowin-box-inner">
					<form>
						<p>Masukan data diri pengguna</p>
						<div class="lowin-group">
							<label>Nama Lengkap</label>
							<input type="text" name="name" autocomplete="name" class="lowin-input">
						</div>
						<div class="lowin-group">
							<label>Email</label>
							<input type="email" autocomplete="email" name="email" class="lowin-input">
						</div>
						<div class="lowin-group">
							<label>Password</label>
							<input type="password" name="password" autocomplete="current-password" class="lowin-input">
						</div>
						<button class="lowin-btn">
							Daftar
						</button>

						<div class="text-foot">
							Sudah memiliki akun? <a href="" class="login-link">Masuk</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	
		<footer class="lowin-footer">
			Build with Heart by <a href="http://lramadhan.gitlab.io/blog">rmdn</a>
		</footer>
	</div>

	<script src="<?php echo base_url('assets/js/auth.js'); ?>"></script>
	<script>
		Auth.init({
			login_url: '#login',
			forgot_url: '#forgot'
		});
	</script>
</body>
</html>