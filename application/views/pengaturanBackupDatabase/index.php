<div class="alert alert-dark alert-dismissible mb-0" role="alert">
    <i class="fa fa-fw fa-info"></i> Disarankan untuk mencandangkan basis data dalam kurun waktu tertentu.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<p class="mb-0">Silakan menekan tombol di bawah untuk mengunduh basis data per tanggal <?php echo date("d/m/Y"); ?>.</p>
<p>
	<a href="pengaturanBackupDatabase/backdb" class="btn btn-primary" role="button"><i class="fa fa-fw fa-download"></i> Unduh</a>
</p>