<div class="card mb-4">
  <div class="card-header bg-white font-weight-bold">
      Data Lengkap Gudang
  </div>
  <div class="card-body">
      <form>
          <div class="form-group row">
              <label for="namaSekolah" class="col-sm-2 col-form-label">Nama Sekolah</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="namaSekolah" placeholder="Tulis nama lengkap sekolah">
              </div>
          </div>
          <div class="form-group row">
              <label for="alamatSekolah" class="col-sm-2 col-form-label">Alamat Sekolah</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="alamatSekolah" placeholder="Tulis alamat lengkap sekolah">
              </div>
          </div>
          <div class="form-group row">
              <label for="namaDivisi" class="col-sm-2 col-form-label">Nama Divisi/Bagian</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="namaDivisi" placeholder="Jurusan/Bagian/Wilayah">
              </div>
          </div>
          <div class="form-group row">
              <label for="nomorTelepon" class="col-sm-2 col-form-label">Nomor Telepon</label>
              <div class="col-sm-10">
                  <input type="tel" class="form-control" id="nomorTelepon" placeholder="089xxxxxxxx">
              </div>
          </div>
          <div class="form-group row">
              <label for="alamatEmail" class="col-sm-2 col-form-label">Alamat Email</label>
              <div class="col-sm-10">
                  <input type="tel" class="form-control" id="alamatEmail" placeholder="user.name@sekolah.sch.id">
              </div>
          </div>
          <div class="form-group row mb-0">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="setuju">
                      <label class="form-check-label" for="setuju">
                          Dengan ini menyetujui dampak dari perubahan data di atas.
                       </label>
                  </div>
              </div>
          </div>
      </form>
  </div>
  <div class="card-footer bg-white">
      <button type="submit" class="btn btn-primary">Simpan</button>
  </div>
</div>