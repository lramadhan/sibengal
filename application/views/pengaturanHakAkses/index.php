<table id="datatables" class="table table-striped" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>ISBN</th>
			<th>Nama Buku</th>
			<th>Penulis</th>
			<th style="width: 123px">Aksi</th>
		</tr>
	</thead>
	<tbody>
<?php $no = 1; foreach ($data_buku as $buku) : ?>
		<tr>
			<td> <?=$no++;?> </td>
			<td> <?=$buku->nama_buku;?> </td>
			<td> <?=$buku->no_isbn;?> </td>
			<td> <?=$buku->penulis;?> </td>
			<td> Oke </td>
		</tr>
<?php endforeach; ?>
	</tbody>
</table>