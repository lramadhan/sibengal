<table id="datatables" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal</th>
      <th>Kode Barang</th>
      <th>Nama Barang</th>
      <th>Keterangan</th>
      <th>Solusi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>15 Jan 2015</td>
      <td>MT020-20</td>
      <td>Monitor Dell 15"</td>
      <td>Pengecekan rutin</td>
      <td>Selesai Diperbarui</td>
    </tr>
    <tr>
      <td>1</td>
      <td>15 Jan 2015</td>
      <td>MT020-22</td>
      <td>Monitor Dell 15"</td>
      <td>Kerusakan Kabel VGA</td>
      <td>Menunggu Pembelian Kabel VGA</td>
    </tr>
  </tbody>
</table>