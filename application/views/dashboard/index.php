            <div class="row mb-4">
                <div class="col-md">
                    <div class="d-flex border">
                        <div class="bg-primary text-light p-4">
                            <div class="d-flex align-items-center h-100">
                                <i class="fa fa-3x fa-fw fa-cog"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 bg-white p-4">
                            <p class="text-uppercase text-secondary mb-0">Alat dan Bahan</p>
                            <h3 class="font-weight-bold mb-0">10%</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="d-flex border">
                        <div class="bg-success text-light p-4">
                            <div class="d-flex align-items-center h-100">
                                <i class="fa fa-3x fa-fw fa-home"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 bg-white p-4">
                            <p class="text-uppercase text-secondary mb-0">Ruangan</p>
                            <h3 class="font-weight-bold mb-0">374</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="d-flex border">
                        <div class="bg-danger text-light p-4">
                            <div class="d-flex align-items-center h-100">
                                <i class="fa fa-3x fa-fw fa-shopping-cart"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 bg-white p-4">
                            <p class="text-uppercase text-secondary mb-0">Transaksi Peminjaman</p>
                            <h3 class="font-weight-bold mb-0">73,829</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="d-flex border">
                        <div class="bg-info text-light p-4">
                            <div class="d-flex align-items-center h-100">
                                <i class="fa fa-3x fa-fw fa-users"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 bg-white p-4">
                            <p class="text-uppercase text-secondary mb-0">Pengguna</p>
                            <h3 class="font-weight-bold mb-0">1,683</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="alert alert-warning" role="alert">
                <p>Selamat datang di Sistem Informasi Manajemen Bengkel Gudang $namaDivisi - $namaSekolah.</p>
                <p><i class="fa fa-fw fa-map-marker"></i> $alamatSekolah</p>
                <p><i class="fa fa-fw fa-phone"></i> $nomorTelepon | <i class="fa fa-fw fa-envelope"></i> $alamatEmail</p>
            </div>