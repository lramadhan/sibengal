<div class="card mb-4 border-primary">
  <div class="card-header bg-primary font-weight-bold text-light">
    Status alat berhasil diperbaiki
  </div>
  <div class="card-body text-default">
      <table id="" class="table table-striped display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Status</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Diperbaiki</td>
            <td>Alat berhasil diperbaiki</td>
          </tr>
        </tbody>
      </table>
  </div>
</div>
<div class="card mb-4 border-warning">
  <div class="card-header bg-warning font-weight-bold">
    Status alat dalam perbaikan
  </div>
  <div class="card-body text-default">
      <table id="" class="table table-striped display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Status</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Diperbaiki</td>
            <td>Alat berhasil diperbaiki</td>
          </tr>
        </tbody>
      </table>
  </div>
</div>
<div class="card mb-4 border-danger">
  <div class="card-header bg-danger font-weight-bold text-light">
    Status alat gagal diperbaiki
  </div>
  <div class="card-body text-default">
      <table id="" class="table table-striped display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Status</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Diperbaiki</td>
            <td>Alat berhasil diperbaiki</td>
          </tr>
        </tbody>
      </table>
  </div>
</div>