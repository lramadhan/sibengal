<div class="alert alert-danger alert-dismissible mb-0" role="alert">
    <i class="fa fa-fw fa-info"></i> Pastikan anda benar-benar ingin mengganti dan menghapus basis data sebelumnya.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<form action="" method="" accept-charset="utf-8">
	
	<div class="form-group">
	<label>Silakan masukkan berkas database (*.sql) <input type="file" name="upload" class="form-control-file"></label>
	</div>
	<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-upload"></i> Unggah</button>
	
</form>