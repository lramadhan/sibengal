<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Model {

	public function get_all_buku()
	{
		$query = $this->db->order_by('id_buku', 'DESC')->get('tbl_buku');
		return $query->result();
	}

}

/* End of file Web.php */
/* Location: ./application/models/Web.php */